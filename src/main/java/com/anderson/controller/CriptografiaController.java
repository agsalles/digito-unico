package com.anderson.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.anderson.service.UsuarioService;

@RestController
@RequestMapping("/api/v1/criptografia")
public class CriptografiaController {
	
	@Autowired
	private UsuarioService usuarioService;
		
	@PutMapping("/chave")
	public ResponseEntity<?> atualizar(@Valid @RequestHeader(value="chave") String chave, @RequestHeader(value="email") String email)  {
		usuarioService.saveChave(chave, email);
		return ResponseEntity.ok().build();
	}

	
}
