package com.anderson.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.anderson.model.Usuario;
import com.anderson.model.dto.UsuarioDTO;
import com.anderson.service.UsuarioService;
import com.anderson.util.UriUtil;

@RestController
@RequestMapping("api/v1/usuarios")
public class UsuarioController {
	
	@Autowired
	private UsuarioService usuarioService;
	
	@GetMapping()
	public ResponseEntity<?> get(){
		List<UsuarioDTO> usuarios = usuarioService.findAll();
		return usuarios.size() > 0 ?
                ResponseEntity.ok(usuarios) :
                ResponseEntity.notFound().build();
	}
	
	@GetMapping("/{id}")
    public ResponseEntity<UsuarioDTO> get(@PathVariable("id") Long id) {
        UsuarioDTO usuario = usuarioService.findById(id);

        return ResponseEntity.ok(usuario);        			         			
    }
	
	@PostMapping
	public ResponseEntity<Usuario> post(@RequestBody Usuario usuario)  {
		
		UsuarioDTO usu = usuarioService.create(usuario);
		
		URI location = UriUtil.getUri(usu.getId());
        return ResponseEntity.created(location).build();
	}
	
	@PutMapping("/{id}")
    public ResponseEntity<?> put(@PathVariable("id") Long id, @RequestBody Usuario usuario) {

        usuario.setId(id);

        UsuarioDTO usu = usuarioService.update(usuario, id);

        return usu != null ?
                ResponseEntity.ok("") :
                ResponseEntity.notFound().build();
    }
	
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        usuarioService.delete(id);

        return ResponseEntity.ok().build();
    }	

    
}
