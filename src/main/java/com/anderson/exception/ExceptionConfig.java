package com.anderson.exception;

import java.io.Serializable;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class ExceptionConfig extends ResponseEntityExceptionHandler {

    @ExceptionHandler({
            EmptyResultDataAccessException.class,
            ObjectNotFoundException.class
    })
    public ResponseEntity<?> errorNotFound(String msg) {
        return ResponseEntity.notFound().build();
    }

    @ExceptionHandler({
           IllegalArgumentException.class
    })
    public ResponseEntity<?> errorBadRequest() {
        return ResponseEntity.badRequest().build();
    }    
    
    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex,
    		HttpHeaders headers, HttpStatus status, WebRequest request) {
    	return new ResponseEntity<>("Operação não permitida", HttpStatus.METHOD_NOT_ALLOWED);
    }
}

class ExceptionError implements Serializable {
	private static final long serialVersionUID = 2484399308157810458L;
	private String error;

	ExceptionError(String error){
    	this.setError(error);
    }

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	
}



