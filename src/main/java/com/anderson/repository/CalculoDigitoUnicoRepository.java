package com.anderson.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.anderson.model.CalculoDigitoUnico;
import com.anderson.model.Usuario;
import java.util.List;

public interface CalculoDigitoUnicoRepository extends JpaRepository<CalculoDigitoUnico, Long>{
		
	List<CalculoDigitoUnico> findByUsuario(Usuario usuario);
	
}
