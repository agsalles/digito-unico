package com.anderson.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.modelmapper.ModelMapper;

import com.anderson.model.dto.CalculoDigitoUnicoDTO;

import lombok.Data;

@Entity
@Data
@Table(name = "calculo_digito_unico")
public class CalculoDigitoUnico {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	private String n;
		
	private Integer k;
		
	private Integer digitoUnico;
	
	@OneToOne(optional = true)
	@JoinColumn(name = "usuario_id", referencedColumnName = "id")
	private Usuario usuario;
	
	public static CalculoDigitoUnico create(CalculoDigitoUnicoDTO calculoDigitoUnicoDTO) {
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(calculoDigitoUnicoDTO, CalculoDigitoUnico.class);
	}
}
