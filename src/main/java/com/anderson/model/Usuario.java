package com.anderson.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Entity
@Data
@Table(name = "usuario")
public class Usuario {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	private String nome;
	
	@NotNull
	private String email;
	
	@JsonIgnore
	@Column(columnDefinition = "varchar(3000)")
	private String chavePublica;
			
	@JsonInclude(JsonInclude.Include.NON_EMPTY) 
	@OneToMany(mappedBy = "usuario", cascade = CascadeType.ALL)	
	private List<CalculoDigitoUnico> calculos = new ArrayList<>();

}
