package com.anderson.model.dto;

import org.modelmapper.ModelMapper;

import com.anderson.model.CalculoDigitoUnico;

import lombok.Data;

@Data
public class CalculoDigitoUnicoDTO {
	
	private Long id;
	private String n;
	private Integer k;
	private Integer digitoUnico;
	private Long   idUsuario;
	private String usuarioEmail;
	private String usuarioNome;
	
	
	public static CalculoDigitoUnicoDTO converter(CalculoDigitoUnico calculoDigitoUnico) {
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(calculoDigitoUnico, CalculoDigitoUnicoDTO.class);
	}
		
	
}
