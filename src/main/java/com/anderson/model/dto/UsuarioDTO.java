package com.anderson.model.dto;

import java.security.PublicKey;
import java.util.List;

import org.modelmapper.ModelMapper;

import com.anderson.model.Usuario;
import com.anderson.util.Criptografia;

import lombok.Data;

@Data
public class UsuarioDTO {
	
	private Long id;
	private String nome;
	private String email;
	private List<CalculoDigitoUnicoDTO> calculos;	
	
    public static UsuarioDTO converter(Usuario usuario) {

		if (usuario.getChavePublica() != null) {
			usuario = criptografar(usuario);
		}
		
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(usuario, UsuarioDTO.class);
    }
    
    private static Usuario criptografar(Usuario usuario) {
    	final PublicKey chavePublica = Criptografia.toPublicKey(usuario.getChavePublica());
    	usuario.setNome(Criptografia.criptografa(usuario.getNome(), chavePublica).toString());
		usuario.setEmail(Criptografia.criptografa(usuario.getEmail(), chavePublica).toString());
		
		return usuario;
    }
}
