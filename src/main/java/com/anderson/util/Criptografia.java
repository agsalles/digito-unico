package com.anderson.util;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import javax.crypto.Cipher;

public class Criptografia {
	
	private static final String ALGORITHM = "RSA";
	
	public static byte[] criptografa(String texto, PublicKey chave) {
	    byte[] cipherText = null;
	     
	    try {
	      final Cipher cipher = Cipher.getInstance(ALGORITHM);
	      cipher.init(Cipher.ENCRYPT_MODE, chave);
	      cipherText = cipher.doFinal(texto.getBytes());
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	     
	    return cipherText;
	  }
	
	public static PublicKey toPublicKey(final String chave)
    {
		KeyFactory kf;
		try {
			kf = KeyFactory.getInstance(ALGORITHM);
			X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(Base64.getDecoder().decode(chave));
	        RSAPublicKey pubKey = (RSAPublicKey) kf.generatePublic(keySpecX509);
	        return pubKey;
		} catch (NoSuchAlgorithmException e1) {
			e1.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
		return null;
    }	
	
	public static String decriptografa(byte[] texto, PrivateKey chave) {
	    byte[] dectyptedText = null;
	     
	    try {
	      final Cipher cipher = Cipher.getInstance(ALGORITHM);
	      cipher.init(Cipher.DECRYPT_MODE, chave);
	      dectyptedText = cipher.doFinal(texto);
	  
	    } catch (Exception ex) {
	      ex.printStackTrace();
	    }
	  
	    return new String(dectyptedText);
	  }
}
