package com.anderson.service;

public interface CacheService {
	
    void add(String key, Integer value);
    
    void remove(String key);
 
    Integer get(String key);
 
    void clear();
 
    long size();

}
