package com.anderson.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.anderson.exception.ObjectNotFoundException;
import com.anderson.model.CalculoDigitoUnico;
import com.anderson.model.Usuario;
import com.anderson.model.dto.CalculoDigitoUnicoDTO;
import com.anderson.repository.CalculoDigitoUnicoRepository;
import com.anderson.repository.UsuarioRepository;
import com.anderson.service.CacheService;
import com.anderson.service.CalculoDigitoUnicoService;

@Service
public class CalculoDigitoUnicoServiceImpl implements CalculoDigitoUnicoService {

	private static final String auxKey = "k:";

	@Autowired
	CalculoDigitoUnicoRepository rep;

	@Autowired
	private UsuarioRepository usuarioRep;

	@Autowired
	private CacheService cacheService;

	@Override
	public List<CalculoDigitoUnicoDTO> findByUsuario(Long id) {

		Usuario usuario = usuarioRep.getOne(id);

		return rep.findByUsuario(usuario).stream().map(CalculoDigitoUnicoDTO::converter).collect(Collectors.toList());

	}

	@Override
	public CalculoDigitoUnicoDTO create(CalculoDigitoUnicoDTO digitoUnicoDto) {

		CalculoDigitoUnico calculoDigitoUnico = CalculoDigitoUnico.create(digitoUnicoDto);
		if (digitoUnicoDto.getIdUsuario() != null) {
			Optional<Usuario> usuarioPresent = usuarioRep.findById(digitoUnicoDto.getIdUsuario());
			
			 if(usuarioPresent.isPresent()) {				 
			    Usuario usuario = usuarioPresent.get();									    
			    calculoDigitoUnico.setUsuario(usuario);							
			  }else {			 
				  throw new ObjectNotFoundException("Usuário não encontrado");
			  }
			 
		}
		
		return CalculoDigitoUnicoDTO.converter(rep.save(calculoDigitoUnico)); 
	}

	@Override
	public Integer calcularDigitoUnico(CalculoDigitoUnicoDTO calculoDigitoUnico) {

		String numero = calculoDigitoUnico.getN();
		Integer k = calculoDigitoUnico.getK();
		String p = "";
		String key = (k != null) ? numero + auxKey + k : numero;

		Integer digitoUnicoCache = cacheService.get(key);
		if (digitoUnicoCache != null) {
			return digitoUnicoCache;
		}

		if (k != null && k > 0) {
			for (int i = 0; i <= k - 1; i++) {
				p += numero;
			}
			numero = p;
		}

		Integer digitoUnico = sum(numero);

		if (cacheService.size() >= 10) {
			cacheService.clear();
		} else {
			cacheService.add(key, digitoUnico);
		}

		return digitoUnico;
	}

	private Integer sum(String numero) {
		String[] n = numero.split("");
		if (n.length == 1)
			return Integer.valueOf(n[0]);

		Integer soma = 0;
		for (int i = 0; i < n.length; i++) {
			soma += Integer.valueOf(n[i]);
		}

		return sum(String.valueOf(soma));
	}
}
