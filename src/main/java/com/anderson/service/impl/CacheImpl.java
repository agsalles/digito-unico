package com.anderson.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.anderson.service.CacheService;

@Service
public class CacheImpl implements CacheService {
	
	private Map<String, Integer> cache = new HashMap<>();

	@Override
	public void add(String key, Integer value) {
		if (key == null) {
			return;
		}
			
		cache.put(key, value);
	}

	@Override
	public void remove(String key) {
		cache.remove(key);
	}

	@Override
	public Integer get(String key) {
		return cache.get(key);
	}

	@Override
	public void clear() {
		cache.clear();
	}

	@Override
	public long size() {
		return cache.size();
	}
	
}
