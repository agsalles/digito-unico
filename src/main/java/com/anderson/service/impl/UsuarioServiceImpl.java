package com.anderson.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.anderson.exception.ObjectNotFoundException;
import com.anderson.model.CalculoDigitoUnico;
import com.anderson.model.Usuario;
import com.anderson.model.dto.UsuarioDTO;
import com.anderson.repository.UsuarioRepository;
import com.anderson.service.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService {
	
	@Autowired
	private UsuarioRepository rep;
	

	@Override
	public List<UsuarioDTO> findAll() {
		return rep.findAll().stream().map(UsuarioDTO::converter).collect(Collectors.toList());
	}
	
	public UsuarioDTO findById(Long id) {
        Optional<Usuario> usuario = rep.findById(id);        
        return usuario.map(UsuarioDTO::converter).orElseThrow(() -> new ObjectNotFoundException("Usuário não encontrado"));
    }
	
	@Override
	public UsuarioDTO create(Usuario usuario) {
		Assert.isNull(usuario.getId(),"Não foi possível inserir o registro");
		
		List<CalculoDigitoUnico> calculos = new ArrayList<>();
		
		for(CalculoDigitoUnico calculo : usuario.getCalculos()) {
			calculo.setUsuario(usuario);
			calculos.add(calculo);
		}
		
		usuario.setCalculos(calculos);
		
		return UsuarioDTO.converter(rep.save(usuario));
	}
	
	public UsuarioDTO update(Usuario usuario, Long id) {
		Assert.notNull(id,"Não foi possível atualizar o registro");

        Optional<Usuario> optional = rep.findById(id);
        if(optional.isPresent()) {
        	Usuario db = optional.get();
            db.setNome(usuario.getNome());
            db.setEmail(usuario.getEmail());
            db.setChavePublica(usuario.getChavePublica());

            rep.save(db);

            return UsuarioDTO.converter(db);
        }else {
    		return null;
        }
        
    }

	@Override
	public void delete(Long id) {
		rep.deleteById(id);
	}


	@Override
	public void saveChave(String chave, String email) {
		Optional<Usuario> optional = rep.findByEmailIgnoreCase(email);
		if(optional.isPresent()) {
			Usuario usuario = optional.get();
			usuario.setChavePublica(chave);			
			rep.save(usuario);
		}else {			
			throw new ObjectNotFoundException("Usuário não encontrado");
		}
	}
	
}

	