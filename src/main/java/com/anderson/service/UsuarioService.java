package com.anderson.service;

import java.util.List;

import com.anderson.model.Usuario;
import com.anderson.model.dto.UsuarioDTO;


public interface UsuarioService {
	
	List<UsuarioDTO> findAll();
	
	UsuarioDTO findById(Long id);

	public UsuarioDTO create(Usuario usuario);
	
	public UsuarioDTO update(Usuario usuario, Long id);

	void delete(Long id);
		
	void saveChave(String chave, String email);
}
