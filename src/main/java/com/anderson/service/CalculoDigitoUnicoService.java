package com.anderson.service;

import java.util.List;

import com.anderson.model.dto.CalculoDigitoUnicoDTO;

public interface CalculoDigitoUnicoService {
	
	
	CalculoDigitoUnicoDTO create(CalculoDigitoUnicoDTO calculoDigitoUnico);
	
	List<CalculoDigitoUnicoDTO> findByUsuario(Long id);
		
	Integer calcularDigitoUnico(CalculoDigitoUnicoDTO calculoDigitoUnicoDTO);
}
