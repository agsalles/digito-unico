package com.anderson.repository;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.anderson.model.CalculoDigitoUnico;
import com.anderson.model.Usuario;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CalculoDigitoUnicoRepositoryTest {
	
	private static final Integer DIGITO_UNICO = 2;
	private static final String N = "8975";
	private static final Integer k = 4;
	private static final Long ID = 1L;
	private static final String NOME = "nome";
	private static final String EMAIL = "teste@teste.com.br";
	private CalculoDigitoUnico calculoDigitoUnico;
	private Usuario usuario;
	
	@Autowired
	private CalculoDigitoUnicoRepository calculoDigitoUnicoRepository;
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Before
	public void init() {
		usuario = new Usuario();
		usuario.setId(ID);
		usuario.setNome(NOME);
		usuario.setEmail(EMAIL);
		usuario = usuarioRepository.save(usuario);
		calculoDigitoUnico = new CalculoDigitoUnico();
		calculoDigitoUnico.setDigitoUnico(DIGITO_UNICO);
		calculoDigitoUnico.setN(N);
		calculoDigitoUnico.setK(k);
		calculoDigitoUnico.setUsuario(usuario);
		calculoDigitoUnico = calculoDigitoUnicoRepository.save(calculoDigitoUnico);
	}
	
    @Test
    public void testBuscarPorUsuario() {
    	List<CalculoDigitoUnico> calculos = calculoDigitoUnicoRepository.findByUsuario(usuario);
    	
    	assertTrue(calculos.size() > 0);    	
    }
    
    
    @After
    public final void finish() {
    	calculoDigitoUnicoRepository.deleteById(calculoDigitoUnico.getId());  
    	usuarioRepository.deleteById(usuario.getId());  
    }
}
