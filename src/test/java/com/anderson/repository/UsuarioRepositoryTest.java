package com.anderson.repository;

import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.anderson.model.CalculoDigitoUnico;
import com.anderson.model.Usuario;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UsuarioRepositoryTest {
	
	private static final Integer DIGITO_UNICO = 2;
	private static final String N = "8975";
	private static final Integer k = 4;
	private static final Long ID = 1L;
	private static final String NOME = "nome";
	private static final String EMAIL = "teste@teste.com.br";
	private CalculoDigitoUnico calculoDigitoUnico;
	private Usuario usuario;
	Optional<Usuario> usuarioOptional;
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Before
	public void init() {
		usuario = new Usuario();
		usuario.setId(ID);
		usuario.setNome(NOME);
		usuario.setEmail(EMAIL);
		calculoDigitoUnico = new CalculoDigitoUnico();
		calculoDigitoUnico.setDigitoUnico(DIGITO_UNICO);
		calculoDigitoUnico.setN(N);
		calculoDigitoUnico.setK(k);
		calculoDigitoUnico.setUsuario(usuario);
		usuario.getCalculos().add(calculoDigitoUnico);
		usuario = usuarioRepository.save(usuario);
	}
	
    @Test
    public void testBuscar() {
    	usuarioOptional = usuarioRepository.findByEmailIgnoreCase(EMAIL);
    	
    	assertTrue(usuarioOptional.isPresent());
    	
    }
    
    @After
    public final void finish() {
    	usuarioRepository.deleteById(usuarioOptional.get().getId());  
    }
}
