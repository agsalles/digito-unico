package com.anderson.service.impl;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.anderson.model.CalculoDigitoUnico;
import com.anderson.model.Usuario;
import com.anderson.model.dto.CalculoDigitoUnicoDTO;
import com.anderson.repository.CalculoDigitoUnicoRepository;
import com.anderson.repository.UsuarioRepository;
import com.anderson.service.CalculoDigitoUnicoService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CalculoDigitoUnicoServiceTest {
	
	private Usuario usuario;
	private CalculoDigitoUnico calculoDigitoUnico;
	private CalculoDigitoUnicoDTO calculoDigitoUnicoDto;
	private static final Long ID = 1L;
	private static final String NOME = "nome";
	private static final String EMAIL = "teste@teste.com.br";
	private static final Integer DIGITO_UNICO = 2;
	private static final String NUMERO = "9875";
	
	
	@MockBean
	private CalculoDigitoUnicoRepository calculoDigitoUnicoRepository;
	
	@MockBean
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private CalculoDigitoUnicoService calculoDigitoUnicoService;
	
	
	@Before
	public void init() throws Exception {
		usuario = new Usuario();
		usuario.setId(ID);
		usuario.setNome(NOME);
		usuario.setEmail(EMAIL);
		calculoDigitoUnico = new CalculoDigitoUnico();
		calculoDigitoUnico.setId(ID);
		calculoDigitoUnico.setN(NUMERO);
		calculoDigitoUnico.setDigitoUnico(DIGITO_UNICO);
		calculoDigitoUnico.setUsuario(usuario);
		
		calculoDigitoUnicoDto = new CalculoDigitoUnicoDTO();
		calculoDigitoUnicoDto.setId(ID);
		calculoDigitoUnicoDto.setN(NUMERO);
		calculoDigitoUnicoDto.setDigitoUnico(DIGITO_UNICO);
		calculoDigitoUnicoDto.setIdUsuario(ID);
	
		BDDMockito.given(usuarioRepository.getOne(Mockito.anyLong())).willReturn(usuario);
		BDDMockito.given(usuarioRepository.findById(Mockito.anyLong())).willReturn(Optional.of(usuario));
		BDDMockito.given(calculoDigitoUnicoRepository.save(Mockito.any(CalculoDigitoUnico.class))).willReturn(calculoDigitoUnico);
	}	
	
	@Test
	public void listarCalculosPorUsuario() {
		
		List<CalculoDigitoUnicoDTO> calculos = calculoDigitoUnicoService.findByUsuario(ID);
		
		assertNotNull(calculos);
	}
	
	@Test
	public void testInsertCalculoDigitoUnico() {
		
		CalculoDigitoUnicoDTO calculoDTO = calculoDigitoUnicoService.create(calculoDigitoUnicoDto);
		assertNotNull(calculoDTO);
	}
		
	@Test
	public void testCalcularDigitoUnico() {
		
		Integer digitoUnico = calculoDigitoUnicoService.calcularDigitoUnico(calculoDigitoUnicoDto);
		
		assertEquals(DIGITO_UNICO, digitoUnico);
		
	}
}
