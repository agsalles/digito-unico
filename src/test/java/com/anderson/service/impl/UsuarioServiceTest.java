package com.anderson.service.impl;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertNotNull;

import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.anderson.exception.ObjectNotFoundException;
import com.anderson.model.Usuario;
import com.anderson.model.dto.UsuarioDTO;
import com.anderson.repository.UsuarioRepository;
import com.anderson.service.UsuarioService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UsuarioServiceTest {
	
	private static final Long ID = 1L;
	private static final String NOME = "nome";
	private static final String EMAIL = "teste@teste.com.br";
	private Usuario usuario;
	
	@MockBean
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Before
	public void init() throws Exception {
		usuario = new Usuario();
		usuario.setNome(NOME);
		usuario.setEmail(EMAIL);
		BDDMockito.given(usuarioRepository.findById(Mockito.anyLong())).willReturn(Optional.of(usuario));
		BDDMockito.given(usuarioRepository.save(Mockito.any(Usuario.class))).willReturn(new Usuario());
	}
	
	@Test
    public void testListar() {

        List<UsuarioDTO> usuarios = usuarioService.findAll();

        assertNotNull(usuarios.size());
    }
	
	@Test
	public void testBuscarUsuarioPorId() {
		UsuarioDTO usuario = usuarioService.findById(ID);
		assertNotNull((usuario));
		assertEquals(NOME, usuario.getNome());
		assertEquals(EMAIL, usuario.getEmail());
	}
	
	@Test
	public void testInsertUsuario() {
		UsuarioDTO usuarioDTO = usuarioService.create(usuario);
		assertNotNull(usuarioDTO);
	}
	
	@Test
	public void testDeleteUsuario() {
		usuarioService.delete(usuario.getId());
        try {
        	usuarioService.findById(usuario.getId());
            fail("O usuário não foi excluído");
        } catch (ObjectNotFoundException e) {
            
        }
	}
}
