# Dígito Único

## Dado um inteiro, o programa encontrar o dígito único do inteiro. Se x tem apenas um dígito, então o seu dígito único é x. Caso contrário, o dígito único de x é igual ao dígito único da soma dos dígitos de x. O dados de e-mail e nome são criptografados.

- Cadastro de Usuários
- Cadastrar Chave
- Cálcular dígito único
- Calculos por usuário
- Listar Usuários
- Listar Usuários por Id
- Atualizar Usuário
- Deletar Usuário

### Arquivo Postman para teste na raiz da aplicação: postman_collection.json

### Instruções

1. Compilar aplicação:
`mvn clean install`
2. Executar os testes:
`mvn test`
3. Executar aplicação:
`cd target`
`java -jar DigitoUnico-1.0.0.jar`

[URL do Swagger](http://localhost:8080/swagger-ui.html "Endereço do Swagger")

## Observações: Inicialmente os dados dos usuários não aparecem criptografados. Para que os dados sejam exibidos criptografados, cadastre primeiramente a chave pública.




